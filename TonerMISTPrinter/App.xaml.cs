﻿using DocPath.PrintServices.Print.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DocPath.PrintServices.Print
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Length == 1)
            {
                string path = e.Args[0];
                if (!File.Exists(path))
                {
                    MessageBox.Show("El archivo no existe.", "DocPath PrintServices", MessageBoxButton.OK, MessageBoxImage.Error);
                    Application.Current.Shutdown(-3);
                }
                else
                {
                    UserCredentials.Instance.File = path;
                    base.OnStartup(e);
                }
            }else
            {
                MessageBox.Show("No se indicó un archivo a imprimir.", "DocPath PrintServices", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown(-2);
            }

        }
    }
}
