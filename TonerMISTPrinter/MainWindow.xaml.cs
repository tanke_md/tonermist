﻿using DocPath.PrintServices.Print.Controller;
using DocPath.PrintServices.Print.Models;
using DocPath.PrintServices.Print;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace DocPath.PrintServices.Print
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            LoadCredentials();
            LoadPrinters();
        }

        private void LoadCredentials()
        {
            LoadCredentialsFromINI();
        }

        private void LoadPrinters()
        {
            UserCredentials uc = UserCredentials.Instance;
            if (uc.Configured && uc.IsLoggedIn)
            {
                PrintServiceController psc = new PrintServiceController();
                List<Device> devices = psc.GetPrinters();
                PopulateDevices(devices);
            }

        }

        public void PopulateDevices(List<Device> devices)
        {
            List<PrinterItem> items = new List<PrinterItem>();
            foreach (Device device in devices)
            {
                items.Add(new PrinterItem() { Name = device.Name, Type = device.AccessType, Id = device.Id });
            }
            lbPrinters.ItemsSource = items;
        }

        #region -- Remove Maximize --

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;
        private const int WS_MAXIMIZEBOX = 0x10000;

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            var hwnd = new WindowInteropHelper((Window)sender).Handle;
            var value = GetWindowLong(hwnd, GWL_STYLE);
            SetWindowLong(hwnd, GWL_STYLE, (int)(value & ~WS_MAXIMIZEBOX));
        }

        #endregion

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            if (lbPrinters.SelectedItem != null)
            {
                UserCredentials uc = UserCredentials.Instance;
                PrinterItem pItem = (PrinterItem)lbPrinters.SelectedItem;
                PrintServiceController psc = new PrintServiceController();
                bool success = psc.Print(uc.File, pItem.Id);
                if (success)
                {
                    MessageBox.Show(string.Format("El documento ha sido enviado correctamente a la impresora {0}.", pItem.Name), "Documento Enviado.", MessageBoxButton.OK, MessageBoxImage.Information);
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("El documento no pudo ser enviado al dispositivo seleccionado.", "Documento No Enviado", MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }

        }

        private void btnConfigure_Click(object sender, RoutedEventArgs e)
        {
            Credentials credentials = new Credentials();
            credentials.ShowDialog();
            UserCredentials uc = UserCredentials.Instance;
            if (uc.Configured && uc.IsLoggedIn)
            {
                EnableButtons();
            }
        }

        //private void btnLoginout_Click(object sender, RoutedEventArgs e)
        //{
        //    if (CheckCredentialsValues())
        //    {
        //        PrintServiceController psc = new PrintServiceController();
        //        bool success = psc.TryLogin(txtUser.Text, txtPassword.Password);
        //        if (success)
        //        {
        //            AddCredentialsToINI();
        //            ShowLoggedPanel();
        //            List<Device> devices = psc.GetPrinters();
        //            PopulateDevices(devices);
        //        }
        //        else
        //        {
        //            ShowNoLoggedPanel();
        //            MessageBox.Show("Invalid credentials.");
        //        }
        //    }

        //}


        #region  -- INI --

        private void LoadCredentialsFromINI()
        {
            INIFileController ini = new INIFileController();
            UserCredentials uc = UserCredentials.Instance;

            uc.UserName = ini.Read("User", "Service");
            uc.Password = ini.Read("Password", "Service");
            if (uc.UserName.Length > 0 && uc.Password.Length > 0)
            {
                uc.Configured = true;
                PrintServiceController psc = new PrintServiceController();
                bool success = psc.TryLogin(uc.UserName, uc.Password);
                uc.IsLoggedIn = success;
                if (!success)
                {
                    MessageBox.Show("It was not possible to connect with the service. Please configure your connection and/or credentials.");
                    DisableButtons();
                }
                else
                {
                    EnableButtons();
                }
            }
            else
            {
                uc.Configured = false;
                MessageBox.Show("You need to configure the product before use it.");
                DisableButtons();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            FileInfo f = new FileInfo(UserCredentials.Instance.File);
            f.Delete();
            System.Windows.Application.Current.Shutdown();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadPrinters();
        }

        #endregion

        private void DisableButtons()
        {
            btnPrint.IsEnabled = false;
            btnRefresh.IsEnabled = false;
        }

        public void EnableButtons()
        {
            btnPrint.IsEnabled = true;
            btnRefresh.IsEnabled = true;
        }
    }

    public class PrinterItem
    {
        public string Name { get; set; }
        public DeviceType Type { get; set; }
        public int Id { get; set; }
        public Visibility ViewFavorite => Type == DeviceType.FAVORITE_PRINTER ? Visibility.Visible : Visibility.Collapsed;
        public Visibility ViewHome => Type == DeviceType.OFFICE ? Visibility.Visible : Visibility.Collapsed;
        public Visibility ViewOffice => Type == DeviceType.OFFICE_PRINTER ? Visibility.Visible : Visibility.Collapsed;
        public Visibility ViewPrinter => Type == DeviceType.AUTHORIZED_PRINTER ? Visibility.Visible : Visibility.Collapsed;

    }
}
