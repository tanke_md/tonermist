﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Helpers
{
    class KeyNoEncryptedException : Exception
    {
        public KeyNoEncryptedException() : base() { }
    }
}
