﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Helpers
{
    class CryptographyHelper
    {

        #region Constants

        private static readonly byte[] Iv = new byte[] { 64, 255, 25, 207, 58, 30, 37, 16, 213, 195, 70, 200, 96, 23, 6, 115 };
        private static readonly string KeyString = "9&7J+V$H.%IT)*#3";

        #endregion


        //private static string TMPRINT_PASSWORD = "password";

        public byte[] TextEncrypted { get; set; }
        public string TextEncryptedString { get; set; }
        public byte[] Key { get; set; }
        //public string KeyString { get; set; }
        public byte[] IV { get; set; }
        public string IVString { get; set; }

        private static string _salt = "aselrias38490a32"; // Random
        private static int _iterations = 65536;
        private static int _keySize = 128;
        private static string _hash = "SHA1";
        private static string _vector = "8947az34awl34kjq"; // Random

        //public string encryptPassword(string pass)
        //{
        //    byte[] encrypted = null;
        //    try
        //    {
        //        byte[] passbytes = System.Text.Encoding.UTF8.GetBytes(TMPRINT_PASSWORD);
        //        byte[] saltbytes = System.Text.Encoding.UTF8.GetBytes(_salt);

        //        //string original = "Here is some data to encrypt!";

        //        // Create a new instance of the RijndaelManaged 
        //        // class.  This generates a new key and initialization  
        //        // vector (IV). 
        //        using (RijndaelManaged myRijndael = new RijndaelManaged())
        //        {

        //            //myRijndael.Padding = PaddingMode.;
        //            myRijndael.Mode = CipherMode.CBC;
        //            myRijndael.KeySize = 128;
        //            //myRijndael.GenerateKey();
        //            myRijndael.GenerateIV();

        //            PasswordDeriveBytes pdb = new PasswordDeriveBytes(passbytes, saltbytes, _hash, _iterations);
        //            TextEncrypted = pdb.GetBytes(_keySize / 8);

        //            IV = myRijndael.IV;
        //            // Encrypt the string to an array of bytes. 
        //            encrypted = EncryptStringToBytes(pass, passbytes /*myRijndael.Key*/, myRijndael.IV);

        //            //// Decrypt the bytes to a string. 
        //            //string roundtrip = DecryptStringFromBytes(encrypted, myRijndael.Key, myRijndael.IV);

        //            ////Display the original data and the decrypted data.
        //            //Console.WriteLine("Original:   {0}", original);
        //            //Console.WriteLine("Round Trip: {0}", roundtrip);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        throw new KeyNoEncryptedException();
        //        //Console.WriteLine("Error: {0}", e.Message);
        //    }

        //    return Encoding.UTF8.GetString(encrypted);
        //}

        //static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        //{
        //    // Check arguments. 
        //    if (plainText == null || plainText.Length <= 0)
        //        throw new ArgumentNullException("plainText");
        //    if (Key == null || Key.Length <= 0)
        //        throw new ArgumentNullException("Key");
        //    if (IV == null || IV.Length <= 0)
        //        throw new ArgumentNullException("IV");
        //    byte[] encrypted;
        //    // Create an RijndaelManaged object 
        //    // with the specified key and IV. 
        //    using (RijndaelManaged rijAlg = new RijndaelManaged())
        //    {
        //        rijAlg.Key = Key;
        //        rijAlg.IV = IV;

        //        // Create a decryptor to perform the stream transform.
        //        ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

        //        // Create the streams used for encryption. 
        //        using (MemoryStream msEncrypt = new MemoryStream())
        //        {
        //            using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
        //            {
        //                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
        //                {

        //                    //Write all data to the stream.
        //                    swEncrypt.Write(plainText);
        //                }
        //                encrypted = msEncrypt.ToArray();
        //            }
        //        }
        //    }


        //    // Return the encrypted bytes from the memory stream. 
        //    return encrypted;

        //}

        private string ConvertToHex(byte[] array)
        {
            string hex = BitConverter.ToString(array).Replace("-", string.Empty);
            return hex;
        }

        public void Encrypt(string mypass)
        {
            Encrypt<AesManaged>(mypass);
        }
        public void Encrypt<T>(string mypass /*, string password*/)
                where T : SymmetricAlgorithm, new()
        {
            //IV = System.Text.Encoding.ASCII.GetBytes(_vector);
            byte[] saltBytes = System.Text.Encoding.ASCII.GetBytes(_salt);
            byte[] valueBytes = System.Text.Encoding.UTF8.GetBytes(mypass);
            //byte[] mypassBytes = System.Text.Encoding.UTF8.GetBytes(mypass);
            byte[] keybytes = System.Text.Encoding.UTF8.GetBytes(KeyString);
            byte[] encrypted;
            using (T cipher = new T())
            {
                //PasswordDeriveBytes _passwordBytes =
                //    new PasswordDeriveBytes(KeyString, saltBytes, _hash, _iterations);
                //byte[] keyBytes = _passwordBytes.GetBytes(_keySize / 8);
                //Key = _passwordBytes.GetBytes(_keySize / 8);

                cipher.Mode = CipherMode.CBC;

                using (ICryptoTransform encryptor = cipher.CreateEncryptor(keybytes, Iv))
                {
                    using (MemoryStream to = new MemoryStream())
                    {
                        using (CryptoStream writer = new CryptoStream(to, encryptor, CryptoStreamMode.Write))
                        {
                            writer.Write(valueBytes, 0, valueBytes.Length);
                            writer.FlushFinalBlock();
                            encrypted = to.ToArray();
                        }
                    }
                }
                cipher.Clear();
            }
            TextEncrypted = encrypted;
            TextEncryptedString = ConvertToHex(encrypted);
            //IVString = ConvertToHex(IV);
            //KeyString = ConvertToHex(Key);
        }





        //#region Properties


        //public string TextEncryptedString { get; set; }


        //public string TextDecryptedString { get; set; }


        //#endregion



        //#region Public Methods

        //public void Encrypt(string value)
        //{
        //    EncryptValue(value);

        //}

        //public void Dencrypt(string value)
        //{
        //    if (!string.IsNullOrEmpty(value)) DecryptValue(value);

        //}


        //#endregion

        //#region Private Methods

        //private void EncryptValue(string value)
        //{


        //    byte[] valueBytes = Encoding.UTF8.GetBytes(value);


        //    var bytesArratyKey = Encoding.UTF8.GetBytes(Key);

        //    var aesCbcPkcs7Algorithm = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
        //    var symmetricKey = aesCbcPkcs7Algorithm.CreateSymmetricKey(bytesArratyKey);

        //    byte[] encrypted;
        //    using (ICryptoTransform encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(symmetricKey, Iv))
        //    {
        //        using (MemoryStream to = new MemoryStream())
        //        {
        //            using (CryptoStream writer = new CryptoStream(to, encryptor, CryptoStreamMode.Write))
        //            {
        //                writer.Write(valueBytes, 0, valueBytes.Length);
        //                writer.FlushFinalBlock();
        //                encrypted = to.ToArray();
        //            }
        //        }
        //    }


        //    TextEncryptedString = ConvertToHex(encrypted);
        //}


        //private void DecryptValue(string value)
        //{

        //    var bytesArratyKey = Encoding.UTF8.GetBytes(Key);
        //    byte[] valueBytes = StringToByteArray(value);

        //    var aesCbcPkcs7Algorithm = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
        //    var symmetricKey = aesCbcPkcs7Algorithm.CreateSymmetricKey(bytesArratyKey);


        //    byte[] decrypted;
        //    using (ICryptoTransform encryptor = WinRTCrypto.CryptographicEngine.CreateDecryptor(symmetricKey, Iv))
        //    {
        //        using (MemoryStream to = new MemoryStream())
        //        {
        //            using (CryptoStream writer = new CryptoStream(to, encryptor, CryptoStreamMode.Write))
        //            {
        //                writer.Write(valueBytes, 0, valueBytes.Length);
        //                writer.FlushFinalBlock();
        //                decrypted = to.ToArray();
        //            }
        //        }
        //    }

        //    string decode = Encoding.UTF8.GetString(decrypted, 0, decrypted.Length);
        //    TextDecryptedString = decode;


        //}

        //private static string ConvertToHex(byte[] array)
        //{
        //    return BitConverter.ToString(array).Replace("-", string.Empty);
        //}

        //private static byte[] StringToByteArray(String hex)
        //{
        //    int NumberChars = hex.Length;
        //    byte[] bytes = new byte[NumberChars / 2];
        //    for (int i = 0; i < NumberChars; i += 2)
        //        bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
        //    return bytes;
        //}




       // #endregion
    }
}
