﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Models
{
    public enum DeviceType
    {
        UNKNOWN,
        FAVORITE_PRINTER,
        OFFICE_PRINTER,
        OFFICE,
        AUTHORIZED_PRINTER
    }
}
