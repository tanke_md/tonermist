﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Models
{
    public class Device
    {
        [JsonProperty("printerAccessType")]
        public String AccessTypeString { get; set; }
        [JsonProperty("printerName")]
        public String Name { get; set; }
        [JsonProperty("printerId")]
        public int Id { get; set; }
        [JsonProperty("printerShareName")]
        public String SharedName { get; set; }
        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }

        public DeviceType AccessType {
            get
            {
                DeviceType type = DeviceType.AUTHORIZED_PRINTER;
                switch (AccessTypeString)
                {
                    case "mainPrinter": type = DeviceType.FAVORITE_PRINTER; break;
                    case "authorized": type = DeviceType.AUTHORIZED_PRINTER; break;
                    case "office": type = DeviceType.OFFICE; break;
                    case "officePrinter": type = DeviceType.OFFICE_PRINTER; break;
                }
                return type;
            }
        }
    }
}
