﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Models
{
    class UserCredentials
    {
        public String UserName { get; set; }
        public String Token { get; set; }
        public String Password { get; set; }
        public bool IsLoggedIn { get; set; }
        public String File { get; set; }
        public Boolean Configured { get; set; }

        private static UserCredentials instance;

        private UserCredentials() { }

        public static UserCredentials Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserCredentials();
                }
                return instance;
            }

        }
    }

}
