﻿using DocPath.PrintServices.Print.Helpers;
using DocPath.PrintServices.Print.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Controller
{
    class PrintServiceController
    {
        public PrintServiceController() { }

        #region -- Connection --

        public string GetConnection()
        {
            string url = GetURL();
            return url;
        }
        

        public string GetURL()
        {
            INIFileController ifc = new INIFileController();
            return ifc.Read("Address", "Service");
        }

        public void SetURL(string url)
        {
            INIFileController ifc = new INIFileController();
            ifc.Write("Address", url, "Service");
        }

        #endregion

        #region -- Login --

        public bool TryLogin(string userName, string password)
        {
            String token = null;
            UserCredentials uc = UserCredentials.Instance;
            uc.UserName = userName;

            CryptographyHelper ch = new CryptographyHelper();
            ch.Encrypt(password);

            try
            {
                WebRequest webRequest = WebRequest.Create(GetConnection() + "/rest/user/login?username=" + userName + "&ct=" + ch.TextEncryptedString);// + "&iv=" + ch.IVString + "&key=" + ch.KeyString);
                WebResponse webResp = webRequest.GetResponse();
                if (webResp.Headers["userAccessToken"] != null)
                {
                    token = webResp.Headers["userAccessToken"];
                    uc.Token = token;
                    uc.IsLoggedIn = true;
                }
            }
            catch (Exception ex)
            {
                uc.IsLoggedIn = false;
                uc.Token = String.Empty;
            }
            return uc.IsLoggedIn;
        }

        #endregion

        #region -- List Printers -- 

        public List<Device> GetPrinters()
        {
            UserCredentials uc = UserCredentials.Instance;

            WebRequest webRequest = WebRequest.Create(GetConnection() + "/rest/user/printers");
            webRequest.Headers.Add("username", uc.UserName);
            webRequest.Headers.Add("userAccessToken", uc.Token);

            WebResponse webResp = webRequest.GetResponse();
            Stream responseStream = webResp.GetResponseStream();
            byte[] bytes = new byte[(int)webResp.ContentLength];
            responseStream.Read(bytes, 0, (int)webResp.ContentLength);

            String text = Encoding.UTF8.GetString(bytes);
            DevicesController dc = new DevicesController();
            List<Device> devicesList = dc.Parse(text);
            return devicesList;
        }

        #endregion

        #region -- Print --

        public bool Print(string file, int printerId)
        {
            bool printed = false;
            UserCredentials uc = UserCredentials.Instance;

            // Read file data
            FileStream fs = new FileStream(@"C:\Users\ulabjga\Documents\Visual Studio 2015\Projects\TonerMISTPrinter\TonerMISTPrinter\testTMPrint.prn", FileMode.Open, FileAccess.Read);
            byte[] data = new byte[fs.Length];
            fs.Read(data, 0, data.Length);
            fs.Close();

            // Generate post objects
            Dictionary<string, object> postParameters = new Dictionary<string, object>();
            postParameters.Add("filename", "testTMPrint.prn");
            postParameters.Add("fileformat", "pcl");
            postParameters.Add("file", new FormUpload.FileParameter(data, "testTMPrint.prn", "application/x-pcl"));

            // Create request and receive response
            string postURL = GetConnection() + "/rest/user/printers/"+ printerId;
            string userAgent = "";
            HttpWebResponse webResponse = null;
            try
            {
                webResponse = FormUpload.MultipartFormDataPost(postURL, userAgent, postParameters);
                printed = true;
            } catch (ServiceException exc)
            {
                printed = false;
            }
            finally
            {
                if (webResponse != null)
                {
                    // Process response
                    StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                    string fullResponse = responseReader.ReadToEnd();
                    webResponse.Close();
                }
            }
            return printed;
        }

        #endregion

    }
}
