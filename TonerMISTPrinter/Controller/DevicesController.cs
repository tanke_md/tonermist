﻿using DocPath.PrintServices.Print.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocPath.PrintServices.Print.Controller
{
    class DevicesController
    {
        public List<Device> Parse(string json)
        {
           List<Device> devices = JsonConvert.DeserializeObject<List<Device>>(json);
           return devices;
        }
    }
}
