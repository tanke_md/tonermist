﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DocPath.PrintServices.Print.Controller;
using DocPath.PrintServices.Print.Models;

namespace DocPath.PrintServices.Print
{
    /// <summary>
    /// Interaction logic for Credentials.xaml
    /// </summary>
    public partial class Credentials : Window
    {
        public Credentials()
        {
            InitializeComponent();
            FillFields();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (CheckCredentialsValues())
            {
                PrintServiceController psc = new PrintServiceController();
                string oldURL = psc.GetURL();
                psc.SetURL(RemoveSlash(txtService.Text));
                bool success = psc.TryLogin(txtUser.Text, txtPassword.Password);
                if (success)
                {
                    INIFileController ifc = new INIFileController();
                    ifc.Write("User", txtUser.Text, "Service");
                    ifc.Write("Password", txtPassword.Password, "Service");
                    ifc.Write("Address", RemoveSlash(txtService.Text), "Service");
                    UserCredentials uc = UserCredentials.Instance;
                    uc.UserName = txtUser.Text;
                    uc.Password = txtPassword.Password;
                    uc.IsLoggedIn = true;
                    uc.Configured = true;
                    this.Hide();
                }
                else
                {
                    psc.SetURL(oldURL);
                    MessageBox.Show("Invalid credentials.");
                }
            }

        }

        private void FillFields()
        {
            INIFileController ifc = new INIFileController();
            string address = ifc.Read("Address", "Service");
            txtService.Text = address;
            string user = ifc.Read("User", "Service");
            txtUser.Text = user;
            string password = ifc.Read("Password", "Service");
            txtPassword.Password = password;
        }

        private bool CheckCredentialsValues()
        {
            bool isValid = true;
            if (txtUser.Text.Length == 0)
            {
                MessageBox.Show("You must introduce an username.");
                isValid = false;
            }
            if (txtUser.Text.Length != 0 && txtPassword.Password.Length == 0)
            {
                MessageBox.Show("You must introduce a password.");
                isValid = false;
            }
            return isValid;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private string RemoveSlash(string url)
        {
            string url2 = url;
            if (url2.EndsWith("/"))
            {
                url2 = url2.Substring(0, url2.Length-1);
            }
            return url2;
        }
    }
}
